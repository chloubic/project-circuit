#pragma once
#include "SFile.h"

namespace ZOS {

	class Defragmentor {

	public:

		void static Defrag(SFile* sfile);

	};

}