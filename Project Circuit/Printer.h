#pragma once
#include <string>
#include <queue>
#include "SFile.h"

namespace ZOS {

	class Printer {

	public:
		void static PrintDirectoryItems(SFile* sfile, std::string path);
		void static SearchAndPrintDirectItems(SFile* sfile, INode* node, int32_t directId);
		void static SearchAndPrintIndirectItems(SFile* sfile, INode* node, int32_t indirectId);
		void static PrintFileContent(SFile* sfile, std::string path);
		void static ReadAndPrintCurrentINodeContent(SFile* sfile);
		void static PrintQueueData(std::queue<int32_t>* dataQueue, long fileSize);
		void static PrintNodeInfo(SFile* sfile, std::string path);
	};

}