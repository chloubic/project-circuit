#pragma once

#include <stdio.h>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <iostream>
#include "INode.h"
#include "SuperBlock.h"

namespace ZOS {

	class SFile {

	private:

		void WriteInitSuperBlock();
		void WriteInitBitMap();
		void WriteInitINodeBlock(INode* initialNode);
		void WriteInitDataBlock();
		void DeleteSFile();

	public:

		int32_t actINodeId;
		std::string actPath;

		std::string fsName;
		std::fstream fsFile;

		SuperBlock* superBlock;

		SFile(std::string fsName);
		~SFile();
		void Initialize();
		void InitFSWithSize(int c_fsSize);
		void WriteFS();
		bool SFileExists();
		void ValidateSFile();

		std::string GetFSName();
		std::string GetPath();

	};

}