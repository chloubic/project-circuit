#include <string>
#include <fstream>
#include "Utils.h"

namespace ZOS {

	bool Utils::FileExists(std::string fileName) {
		std::ifstream f(fileName.c_str());
		return f.good();
	}

}