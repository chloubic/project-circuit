#pragma once
#include "DirectoryItem.h"

#define DEFAULT_CHAR 0xFF;

// Initial size - 20 MB
#define FS_INIT_SIZE 20971520

#define STRING_END '\0'

#define DATA_C_SIZE 1024
#define LINK_C 1024 / sizeof(DirectoryItem)
#define IND_LINK_C 1024 / sizeof(int32_t)

#define MAX_LENGTH_OF_COMMAND 100
#define SHELL_CHAR "$"
#define ROOT_CHAR "~"
#define END_CLI_CHAR ">"
#define SPLIT_ARGS_CHAR " "
#define SIGNATURE "chlouba"
#define VOLUME_DESCRIPTOR "Volume descriptor"
#define DIR_CHAR "/"
#define EMPTY_CHAR '0'

#define DIR_SYMBOL " + "
#define FILE_SYMBOL " - "

#define KILO 1024
#define MEGA 1024 * 1024

#define COPY_COMMAND "cp"
#define MOVE_COMMAND "mv"
#define REMOVE_COMMAND "rm"
#define MAKE_DIR_COMMAND "mkdir"
#define REMOVE_DIR_COMMAND "rmdir"
#define LIST_COMMAND "ls"
#define PRINT_COMMAND "cat"
#define CHANGE_DIR_COMMAND "cd"
#define PRINT_WORKING_DIR_COMMAND "pwd"
#define FS_INFO_COMMAND "info"
#define IMPORT_COMMAND "incp"
#define EXPORT_COMMAND "outcp"
#define LOAD_CMDS_COMMAND "load"
#define FORMAT_COMMAND "format"
#define DEFRAG_COMMAND "defrag"
#define EXIT_COMMAND "exit"

#define OK_RESULT "OK"
#define FILE_NOT_FOUND_RESULT "FILE NOT FOUND"
#define CANNOT_CR_FILE_RESULT "CANNOT CREATE FILE"
#define PATH_NOT_FOUND "PATH NOT FOUND"
#define EXISTS_RESULT "EXISTS"
#define NOT_EMPTY_RESULT "NOT EMPTY"

#define ROOT_NODE_ID 1

#define FREE 0
#define DIR 1
#define FILES 2
#define OTHER 3

#define L1 1
#define L2 2
#define L3 3
#define L4 4
#define L5 5

#define L_COUNT 5
#define L_IN_COUNT 2

#define DEFAULT_BITMAP_VALUE "0"
#define DEFAULT_BITMAP_VALUE_STREAM_SIZE 1

#define WRONG_ARGS -1
#define FS_FILE_EXCEPTION -2