#pragma once
#include <string>
#include "SFile.h"
#include "DirectoryItem.h"

namespace ZOS {

	class Director {

	public:
		void static Make(SFile* sfile, std::string path);
		void static Remove(SFile* sfile, std::string path);
		void static InitPrintingDirectoryItem(ZOS::SFile* sfile, DirectoryItem* item);
		DirectoryItem static DeleteDirectLinks(SFile* sfile, int32_t iNodeId, INode* node, int32_t directId);
		DirectoryItem static DeleteIndirectLinks(SFile* sfile, int32_t iNodeId, INode* node, int32_t indirectId);
	};

}