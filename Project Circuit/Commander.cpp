#include <string>
#include <iostream>
#include <list>
#include "Commander.h"
#include "Constants.h"
#include "Convertor.h"
#include "INode.h"
#include "Splitter.h"

#include "Formatter.h"
#include "Loader.h"
#include "Defragmentor.h"
#include "SFile.h"
#include "Filer.h"
#include "Director.h"
#include "Printer.h"

namespace ZOS {
	
	Commander::Commander(SFile* c_sfile) {
		sfile = c_sfile;
		runner = false;
	}

	void Commander::PrintCLIChars() {
		std::cout << sfile->GetFSName() << SHELL_CHAR << " " << sfile->GetPath() << END_CLI_CHAR;
	}

	void Commander::Execute() {
		this->runner = true;
		std::string command;
		while (this->runner) {
			this->PrintCLIChars();
			getline(std::cin, command);
			this->RunCommand(command);
		}
	}

	void Commander::RunCommand(std::string command) {
		std::list<std::string> commandVector = split(command, SPLIT_ARGS_CHAR);
		if (commandVector.front().compare(COPY_COMMAND) == 0 && commandVector.size() == 3)
			this->ExecuteCopyCommand(commandVector);
		else if (commandVector.front().compare(MOVE_COMMAND) == 0 && commandVector.size() == 3)
			this->ExecuteMoveCommand(commandVector);
		else if (commandVector.front().compare(REMOVE_COMMAND) == 0 && commandVector.size() == 2)
			this->ExecuteRemoveCommand(commandVector);
		else if (commandVector.front().compare(MAKE_DIR_COMMAND) == 0 && commandVector.size() == 2)
			this->ExecuteMakeDirCommand(commandVector);
		else if (commandVector.front().compare(REMOVE_DIR_COMMAND) == 0 && commandVector.size() == 2)
			this->ExecuteRemoveDirCommand(commandVector);
		else if (commandVector.front().compare(LIST_COMMAND) == 0 && (commandVector.size() == 2 || commandVector.size() == 1))
			this->ExecuteListCommand(commandVector);
		else if (commandVector.front().compare(PRINT_COMMAND) == 0 && commandVector.size() == 2)
			this->ExecutePrintCommand(commandVector);
		else if (commandVector.front().compare(CHANGE_DIR_COMMAND) == 0 && commandVector.size() == 2)
			this->ExecuteChangeDirCommand(commandVector);
		else if (commandVector.front().compare(PRINT_WORKING_DIR_COMMAND) == 0 && commandVector.size() == 1)
			this->ExecutePrintWorkingDirCommand(commandVector);
		else if (commandVector.front().compare(FS_INFO_COMMAND) == 0 && commandVector.size() == 2)
			this->ExecuteFSInfoCommand(commandVector);
		else if (commandVector.front().compare(IMPORT_COMMAND) == 0 && commandVector.size() == 3)
			this->ExecuteImportCommand(commandVector);
		else if (commandVector.front().compare(EXPORT_COMMAND) == 0 && commandVector.size() == 3)
			this->ExecuteExportCommand(commandVector);
		else if (commandVector.front().compare(LOAD_CMDS_COMMAND) == 0 && commandVector.size() == 2)
			this->ExecuteLoadCmdsCommand(commandVector);
		else if (commandVector.front().compare(FORMAT_COMMAND) == 0 && (commandVector.size() == 2 || commandVector.size() == 3))
			this->ExecuteFormatCommand(commandVector);
		else if (commandVector.front().compare(DEFRAG_COMMAND) == 0 && commandVector.size() == 1)
			this->ExecuteDefragCommand();
		else if (commandVector.front().compare(EXIT_COMMAND) == 0 && commandVector.size() == 1)
			this->ExecuteExitCommand();
		else
			this->ExecuteCommandNotFound(command);
	}

	void Commander::ExecuteCopyCommand(std::list<std::string> command) {
		Filer::Copy(
			sfile,
			RelativePathToAbsolute(sfile, *std::next(command.begin(), 1)),
			RelativePathToAbsolute(sfile, *std::next(command.begin(), 2))
		);
	}

	void Commander::ExecuteMoveCommand(std::list<std::string> command) {
		Filer::Move(
			sfile,
			RelativePathToAbsolute(sfile, *std::next(command.begin(), 1)),
			RelativePathToAbsolute(sfile, *std::next(command.begin(), 2))
		);
	}

	void Commander::ExecuteRemoveCommand(std::list<std::string> command) {
		Filer::Remove(sfile, RelativePathToAbsolute(sfile, *std::next(command.begin(), 1)));
	}

	void Commander::ExecuteMakeDirCommand(std::list<std::string> command) {
		Director::Make(sfile, RelativePathToAbsolute(sfile, *std::next(command.begin(), 1)));
	}

	void Commander::ExecuteRemoveDirCommand(std::list<std::string> command) {
		Director::Remove(sfile, RelativePathToAbsolute(sfile, *std::next(command.begin(), 1)));
	}

	void Commander::ExecuteListCommand(std::list<std::string> command) {
		if (command.size() == 2)
			Printer::PrintDirectoryItems(sfile, RelativePathToAbsolute(sfile, *std::next(command.begin(), 1)));
		else
			Printer::PrintDirectoryItems(sfile, sfile->actPath);
	}

	void Commander::ExecutePrintCommand(std::list<std::string> command) {
		Printer::PrintFileContent(sfile, RelativePathToAbsolute(sfile, *std::next(command.begin(), 1)));
	}

	void Commander::ExecuteChangeDirCommand(std::list<std::string> command) {
		int32_t oldNodeId = sfile->actINodeId;
		std::string path = RelativePathToAbsolute(sfile, command.back());
		if (path.compare("") == 0) return;
		sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::out | std::ios::in);
		if (INodeExists(sfile, ROOT_NODE_ID, DIR, path)) {
			sfile->actPath = path;
			std::cout << OK_RESULT << std::endl;
		}
		else {
			sfile->actINodeId = oldNodeId;
			std::cout << PATH_NOT_FOUND << std::endl;
		}
		sfile->fsFile.close();
	}

	void Commander::ExecutePrintWorkingDirCommand(std::list<std::string> command) {
		std::cout << this->sfile->GetPath() << std::endl;
	}

	void Commander::ExecuteFSInfoCommand(std::list<std::string> command) {
		Printer::PrintNodeInfo(sfile, RelativePathToAbsolute(sfile, *std::next(command.begin(), 1)));
	}

	void Commander::ExecuteImportCommand(std::list<std::string> command) {
		Filer::Import(sfile, *std::next(command.begin(), 1), RelativePathToAbsolute(sfile, *std::next(command.begin(), 2)));
	}

	void Commander::ExecuteExportCommand(std::list<std::string> command) {
		Filer::Export(sfile, RelativePathToAbsolute(sfile, *std::next(command.begin(), 1)), *std::next(command.begin(), 2));
	}

	void Commander::ExecuteLoadCmdsCommand(std::list<std::string> command) {
		if (command.size() == 2) {
			Loader::Load(this, *std::next(command.begin(), 1));
		}
	}

	void Commander::ExecuteFormatCommand(std::list<std::string> command) {
		int bytes = -1;
		if (command.size() == 2) {
			std::string numbers = "";
			int charBreak = -1;
			for (unsigned int i = 0; i < std::next(command.begin(), 1)->length(); i++) {
				if (std::next(command.begin(), 1)->at(i) >= 48 && std::next(command.begin(), 1)->at(i) <= 57)
					if (i == 0 && std::next(command.begin(), 1)->at(i) == 48) break;
					else numbers += std::next(command.begin(), 1)->at(i);
				else {
					charBreak = i;
					break;
				}
			}
			int multiplier = charBreak > 0 ? 
				this->ExtractMultiplier(std::next(command.begin(), 1)->substr(charBreak, 2)) : 1;
			try {
				int bytesBase = std::stoi(numbers);
				bytes = bytesBase * multiplier;
			}
			catch(const std::exception &e){}

		}
		else if (command.size() == 3) {
			try {
				int bytesBase = std::stoi(*std::next(command.begin(), 1));
				int multiplier = this->ExtractMultiplier(*std::next(command.begin(), 2));
				bytes = bytesBase * multiplier;
			}
			catch(const std::exception &e){}
		}
		if (bytes > 0) Formatter::Format(sfile, bytes);
	}

	void Commander::ExecuteDefragCommand() {
		Defragmentor::Defrag(sfile);
	}

	void Commander::ExecuteExitCommand() {
		this->runner = false;
	}

	void Commander::ExecuteCommandNotFound(std::string command) {
		std::cout << command << ": Command not found" << std::endl;
	}

	int Commander::ExtractMultiplier(std::string fileSizeFormat) {
		if (fileSizeFormat.length() == 1) {
			if (fileSizeFormat.at(0) == 66 || fileSizeFormat.at(0) == 98)
				return 1;
		}
		if (fileSizeFormat.length() == 2) {
			if (fileSizeFormat.at(1) == 66 || fileSizeFormat.at(1) == 98) {
				if (fileSizeFormat.at(0) == 107 || fileSizeFormat.at(0) == 75)
					return KILO;
				else if (fileSizeFormat.at(0) == 109 || fileSizeFormat.at(0) == 77)
					return MEGA;
			}
		}
		return 0;
	}

}