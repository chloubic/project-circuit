#pragma once
#include <queue>
#include "SFile.h"
#include "DirectoryItem.h"
#include "INode.h"

namespace ZOS {

	class Magician {

	public:
		void static WriteDataIntoCluster(SFile* sfile, INode* node, std::queue<int32_t>* dataQueue, bool isIndirect, int32_t linkId);
		void static CreateIndirectForImportedFile(SFile* sfile, INode* node, int32_t linkId);
		bool static LoadLinkDataIntoQueue(SFile* sfile, INode* node, std::queue<int32_t>* dataQueue, bool isIndirect, int32_t linkId);
		void static LoadIndirectLinks(SFile* sfile, std::queue<int32_t>* dataQueue, int32_t linkId);
		DirectoryItem static DeleteDirectLinks(SFile* sfile, int32_t iNodeId, INode* node, int32_t directId);
		DirectoryItem static DeleteIndirectLinks(SFile* sfile, int32_t iNodeId, INode* node, int32_t indirectId);
		void static RemoveINodes(SFile* sfile, int32_t iNodeId);
		void static RemoveDirects(SFile* sfile, INode* node, int32_t directId);
		void static RemoveIndirects(SFile* sfile, INode* node, int32_t indirectId);
		DirectoryItem static TakeFile(SFile* sfile, int32_t nodeId);
		void static DuplicateNodes(SFile* sfile, int32_t iNodeIdFrom, int32_t iNodeIdTo, std::string name);
		void static LoadClusterDataIntoQueue(SFile* sfile, std::queue<int32_t>* dataQueue, int32_t clusterId);
		void static LoadINodesIntoList(SFile* sfile, std::list<INode>* nodesList);
		void static SortClusterData(SFile* sfile, std::queue<int32_t>* oldDataQueue, std::queue<int32_t>* newDataQueue, int32_t oldClusterId, int32_t newClusterId);
		void static SortINodeData(SFile* sfile, std::list<INode>* nodesList, int32_t oldNodeId, int32_t newNodeId);
		bool static IsDirectClusterClear(SFile* sfile, int32_t directId);

	};

}