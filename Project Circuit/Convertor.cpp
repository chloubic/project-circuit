#include <string>
#include <iostream>
#include <list>
#include "Convertor.h"
#include "DirectoryItem.h"
#include "SFile.h"
#include "Splitter.h"

void FillName(DirectoryItem* c_dirItem, std::string c_name) {
	for (unsigned int i = 0; i < DIR_NAME_LENGHT; i++) {
		if (i < c_name.length()) c_dirItem->itemName[i] = c_name.at(i);
		else c_dirItem->itemName[i] = STRING_END;
	}
}

std::string RelativePathToAbsolute(ZOS::SFile* sfile, std::string path) {
    std::list<std::string> parsedActDir = split(sfile->GetPath(), "/");
    std::list<std::string> parsedRelPath = split(path, "/");

    if (parsedActDir.size() > 0 && parsedActDir.back().compare("") == 0) parsedActDir.pop_back();

    if (parsedRelPath.front().compare("~") == 0) {
        return path;
    }
    else {
        std::list<std::string>::iterator it;
        for (it = parsedRelPath.begin(); it != parsedRelPath.end(); ++it) {
            if (!(*it).compare("") == 0 && !(*it).compare(".") == 0) {
                if ((*it).compare("..") == 0) {
                    if (parsedActDir.size() > 1) parsedActDir.pop_back();
                    else {
                        parsedActDir.pop_back();
                        parsedActDir.push_back(ROOT_CHAR);
                        break;
                    }
                }
                else parsedActDir.push_back((*it));
            }
        }

        std::string finalPath = "";
        for (it = parsedActDir.begin(); it != parsedActDir.end(); ++it) finalPath.append((*it) + "/");
        return finalPath;
    }
}

bool INodeExists(ZOS::SFile* sfile, int32_t iNodeId, int nodeType, std::string path) {
    INode node;
    DirectoryItem dir;
    int32_t position = 0;
    std::list<std::string> splittedPath = split(path, DIR_CHAR);
    if (splittedPath.size() > 0 && splittedPath.back().compare("") == 0) splittedPath.pop_back();
    if (splittedPath.size() > 0 && splittedPath.front().compare("~") == 0) splittedPath.pop_front();
    if (splittedPath.size() < 1) {
        node = GetFileNode(sfile, iNodeId);
        if (nodeType == FILES && node.isDirectory) return false;
        sfile->actINodeId = iNodeId;
        return true;
    }
    std::string nextPathName = splittedPath.front();
    splittedPath.pop_front();
    node = GetFileNode(sfile, iNodeId);
    if (nodeType == DIR && !node.isDirectory) return false;
    std::string actItemName;
    if (node.direct1 != 0) {
        for (int i = 0; i < LINK_C; i++) {
            dir = GetFileDirectoryItem(sfile, node.direct1, i);
            actItemName = dir.itemName;
            if (nextPathName.compare(actItemName) == 0) return INodeExists(sfile, dir.inode, nodeType, serialize(splittedPath));
        }
    }
    if (node.direct2 != 0) {
        for (int i = 0; i < LINK_C; i++) {
            dir = GetFileDirectoryItem(sfile, node.direct2, i);
            actItemName = dir.itemName;
            if (nextPathName.compare(actItemName) == 0) return INodeExists(sfile, dir.inode, nodeType, serialize(splittedPath));
        }
    }
    if (node.direct3 != 0) {
        for (int i = 0; i < LINK_C; i++) {
            dir = GetFileDirectoryItem(sfile, node.direct3, i);
            actItemName = dir.itemName;
            if (nextPathName.compare(actItemName) == 0) return INodeExists(sfile, dir.inode, nodeType, serialize(splittedPath));
        }
    }
    if (node.direct4 != 0) {
        for (int i = 0; i < LINK_C; i++) {
            dir = GetFileDirectoryItem(sfile, node.direct4, i);
            actItemName = dir.itemName;
            if (nextPathName.compare(actItemName) == 0) return INodeExists(sfile, dir.inode, nodeType, serialize(splittedPath));
        }
    }
    if (node.direct5 != 0) {
        for (int i = 0; i < LINK_C; i++) {
            dir = GetFileDirectoryItem(sfile, node.direct5, i);
            actItemName = dir.itemName;
            if (nextPathName.compare(actItemName) == 0) return INodeExists(sfile, dir.inode, nodeType, serialize(splittedPath));
        }
    }
    if (node.indirect1 != 0) {
        for (int i = 0; i < IND_LINK_C; i++) {
            position = GetFileIndirectId(sfile, node.indirect1, i);
            if (position > 0) {
                for (int j = 0; j < LINK_C; j++) {
                    dir = GetFileDirectoryItem(sfile, position, j);
                    actItemName = dir.itemName;
                    if (nextPathName.compare(actItemName) == 0) return INodeExists(sfile, dir.inode, nodeType, serialize(splittedPath));
                }
            }
        }
    }
    if (node.indirect2 != 0) {
        for (int i = 0; i < IND_LINK_C; i++) {
            position = GetFileIndirectId(sfile, node.indirect2, i);
            if (position > 0) {
                for (int j = 0; j < LINK_C; j++) {
                    dir = GetFileDirectoryItem(sfile, position, j);
                    actItemName = dir.itemName;
                    if (nextPathName.compare(actItemName) == 0) return INodeExists(sfile, dir.inode, nodeType, serialize(splittedPath));
                }
            }
        }
    }
    return false;
}

INode GetFileNode(ZOS::SFile* sfile, int iNodeId) {
    INode node;
    sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (iNodeId - 1) * sizeof(INode));
    sfile->fsFile.read((char*)&node, sizeof(INode));
    return node;
}

DirectoryItem GetFileDirectoryItem(ZOS::SFile* sfile, int directId, int blockPosition) {
    DirectoryItem dir;
    sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (directId - 1) * DATA_C_SIZE + blockPosition * sizeof(DirectoryItem));
    sfile->fsFile.read((char*)&dir, sizeof(DirectoryItem));
    return dir;
}

int32_t GetFileIndirectId(ZOS::SFile* sfile, int indirectId, int blockPosition) {
    int32_t nodeId;
    sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (indirectId - 1) * DATA_C_SIZE + blockPosition * sizeof(int32_t));
    sfile->fsFile.read((char*)&nodeId, sizeof(int32_t));
    return nodeId;
}

bool FindDirectLink(ZOS::SFile* sfile, int32_t linkType, INode* node, DirectoryItem* item) {
    int32_t linkId = FREE;
    DirectoryItem* dirItem = new DirectoryItem;
    if (linkType == L1) linkId = node->direct1;
    else if (linkType == L2) linkId = node->direct2;
    else if (linkType == L3) linkId = node->direct3;
    else if (linkType == L4) linkId = node->direct4;
    else if (linkType == L5) linkId = node->direct5;

    if (linkId != FREE) {
        sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (linkId - 1) * DATA_C_SIZE);
        for (int i = 0; i < LINK_C; i++) {
            sfile->fsFile.read((char*) dirItem, sizeof(DirectoryItem));
            if (dirItem->inode == FREE) {
                sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (linkId - 1) * DATA_C_SIZE + i * sizeof(DirectoryItem));
                sfile->fsFile.write((char*) item, sizeof(DirectoryItem));
                sfile->fsFile.flush();
                return true;
            }
        }
    }
    else {
        WriteNewLink(sfile, linkType, node, item);
        return true;
    }

    return false;
}

bool FindIndirectLink(ZOS::SFile* sfile, int32_t linkType, INode* node, DirectoryItem* item) {
    int32_t link;
    DirectoryItem existing_item;
    if (linkType == L1) link = node->indirect1;
    else if (linkType == L2) link = node->indirect2;

    if (link != 0) {
        int32_t position = 0;
        for (int j = 0; j < IND_LINK_C; j++) {
            sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (link - 1) * DATA_C_SIZE + j * sizeof(int32_t));
            sfile->fsFile.read((char*)&position, sizeof(int32_t));
            if (position > 0) {
                for (int i = 0; i < LINK_C; i++) {
                    sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (position - 1) * DATA_C_SIZE + i * sizeof(DirectoryItem));
                    sfile->fsFile.read((char*)&existing_item, sizeof(DirectoryItem));
                    if (existing_item.inode == 0) {
                        sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (position - 1) * DATA_C_SIZE + i * sizeof(DirectoryItem));
                        sfile->fsFile.write((char*) item, sizeof(DirectoryItem));
                        return true;
                    }
                }
            }
            else {
                WriteIndirectItem(sfile, linkType, j, item);
                return true;
            }
        }
    }
    else {
        sfile->fsFile.seekg(sfile->superBlock->bitmapStartAddress);
        unsigned char empty;
        int emptyClusterIndex;
        for (int i = 0; i < sfile->superBlock->clusterCount; i++) {
            sfile->fsFile.read((char*)&empty, 1);
            if (empty == '0') {
                sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + i);
                sfile->fsFile.write("1", 1);
                sfile->fsFile.flush();
                emptyClusterIndex = i + 1;
                break;
            }
        }
        if (linkType == L1) node->indirect1 = emptyClusterIndex;
        else if (linkType == L2) node->indirect2 = emptyClusterIndex;
        sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node->nodeId - 1) * sizeof(INode));
        sfile->fsFile.write((char*) &node, sizeof(INode));
        WriteIndirectItem(sfile, linkType, 0, item);
    }

    return false;
}

void WriteNewLink(ZOS::SFile* sfile, int32_t linkType, INode* node, DirectoryItem* item) {
    sfile->fsFile.seekg(sfile->superBlock->bitmapStartAddress);
    int emptyClusterIndex = FindEmptyCluster(sfile);
    int address = sfile->superBlock->dataStartAddress + (emptyClusterIndex - 1) * sfile->superBlock->clusterSize;
    WriteDirectoryItem(sfile, item, address);
    if (linkType == L1) node->direct1 = emptyClusterIndex;
    else if (linkType == L2) node->direct2 = emptyClusterIndex;
    else if (linkType == L3) node->direct3 = emptyClusterIndex;
    else if (linkType == L4) node->direct4 = emptyClusterIndex;
    else if (linkType == L5) node->direct5 = emptyClusterIndex;
    sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node->nodeId - 1) * sizeof(INode));
    sfile->fsFile.write((char*) node, sizeof(INode));
    sfile->fsFile.flush();
}

int FindEmptyCluster(ZOS::SFile* sfile) {
    sfile->fsFile.seekg(sfile->superBlock->bitmapStartAddress);
    unsigned char charType;
    for (int i = 0; i < sfile->superBlock->clusterCount; i++) {
        sfile->fsFile.read((char*) &charType, 1);
        if (charType == '0') {
            sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + i);
            sfile->fsFile.write("1", 1);
            sfile->fsFile.flush();
            return i + 1;
        }
    }
    return 0;
}

void WriteDirectoryItem(ZOS::SFile* sfile, DirectoryItem* item, int address) {
    DirectoryItem* dirItem = new DirectoryItem;
    dirItem->inode = 0;
    for (int i = 0; i < DIR_NAME_LENGHT; i++) dirItem->itemName[i] = '\0';
    sfile->fsFile.seekp(address);
    sfile->fsFile.write((char*) item, sizeof(DirectoryItem));
    for (int j = 1; j < LINK_C; j++) {
        sfile->fsFile.write((char*) dirItem, sizeof(DirectoryItem));
    }
    sfile->fsFile.flush();
}

void WriteIndirectItem(ZOS::SFile* sfile, int linkType, int linkIndex, DirectoryItem* item) {
    int emptyClusterIndex = FindEmptyCluster(sfile);
    sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (linkType - 1) * sfile->superBlock->clusterSize + linkIndex * sizeof(int32_t));
    sfile->fsFile.read((char*) &emptyClusterIndex, sizeof(int32_t));
    int address = sfile->superBlock->dataStartAddress + (emptyClusterIndex - 1) * sfile->superBlock->clusterSize;
    WriteDirectoryItem(sfile, item, address);
}