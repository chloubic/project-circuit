#include <string>
#include <queue>
#include <list>
#include "Printer.h"
#include "SFile.h"
#include "Constants.h"
#include "Convertor.h"
#include "Magician.h"
#include "Splitter.h"

namespace ZOS {

	void Printer::PrintDirectoryItems(SFile* sfile, std::string path) {
        if (path.compare("") == 0) {
            std::cout << PATH_NOT_FOUND << std::endl;
            return;
        }
        sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::out | std::ios::in);
        int currentINodeId = sfile->actINodeId;
        if (!INodeExists(sfile, ROOT_NODE_ID, DIR, path)) {
            std::cout << PATH_NOT_FOUND << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        INode* dirINode = new INode;
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (sfile->actINodeId - 1) * sizeof(INode));
        sfile->fsFile.read((char*) dirINode, sizeof(INode));
        for (int i = 1; i <= L5; i++) SearchAndPrintDirectItems(sfile, dirINode, i);
        for (int i = 1; i <= L2; i++) SearchAndPrintIndirectItems(sfile, dirINode, i);
        sfile->actINodeId = currentINodeId;
        sfile->fsFile.close();
	}

    void Printer::SearchAndPrintDirectItems(SFile* sfile, INode* node, int32_t directId) {
        int32_t directLinkId = FREE;
        DirectoryItem* dirItem = new DirectoryItem;
        INode* iNode = new INode;
        if (directId == L1) directLinkId = node->direct1;
        else if (directId == L2) directLinkId = node->direct2;
        else if (directId == L3) directLinkId = node->direct3;
        else if (directId == L4) directLinkId = node->direct4;
        else if (directId == L5) directLinkId = node->direct5;
        if (directLinkId != 0) {
            for (unsigned int i = 0; i < LINK_C; i++) {
                sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (directLinkId - 1) * DATA_C_SIZE + i * sizeof(DirectoryItem));
                sfile->fsFile.read((char*) dirItem, sizeof(DirectoryItem));
                if (dirItem->inode != FREE) {
                    sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (dirItem->inode - 1) * sizeof(INode));
                    sfile->fsFile.read((char*) iNode, sizeof(INode));
                    if (iNode->isDirectory) std::cout << DIR_SYMBOL;
                    else std::cout << FILE_SYMBOL;
                    std::cout << dirItem->itemName << std::endl;
                }
            }
        }
    }

    void Printer::SearchAndPrintIndirectItems(SFile* sfile, INode* node, int32_t indirectId) {
        int32_t indirectLinkId = FREE;
        DirectoryItem* dirItem = new DirectoryItem;
        INode* iNode = new INode;

        if (indirectId == L1) indirectLinkId = node->indirect1;
        else if (indirectId == L2) indirectLinkId = node->indirect2;
        if (indirectLinkId != FREE) {
            int32_t iNodeIndex = FREE;
            for (int i = 0; i < IND_LINK_C; i++) {
                sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (indirectLinkId - 1) * DATA_C_SIZE + i * sizeof(int32_t));
                sfile->fsFile.read((char*) iNodeIndex, sizeof(int32_t));
                if (iNodeIndex > FREE) {
                    for (int j = 0; j < LINK_C; j++) {
                        sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (iNodeIndex - 1) * DATA_C_SIZE + j * sizeof(DirectoryItem));
                        sfile->fsFile.read((char*) dirItem, sizeof(DirectoryItem));
                        if (dirItem->inode != FREE) {
                            sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (dirItem->inode - 1) * sizeof(INode));
                            sfile->fsFile.read((char*) iNode, sizeof(INode));
                            if (iNode->isDirectory) std::cout << DIR_SYMBOL;
                            else std::cout << FILE_SYMBOL;
                            std::cout << dirItem->itemName << std::endl;
                        }
                    }
                }
            }
        }
    }

    void Printer::PrintFileContent(SFile* sfile, std::string path) {
        if (path.compare("") == 0) {
            std::cout << PATH_NOT_FOUND << std::endl;
            return;
        }
        sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::out | std::ios::in);
        int currentINodeId = sfile->actINodeId;
        std::list<std::string> splittedPath = split(path, DIR_CHAR);
        if (splittedPath.size() > 0 && splittedPath.back().compare("") == 0) splittedPath.pop_back();
        if (splittedPath.size() > 0 && splittedPath.front().compare("~") == 0) splittedPath.pop_front();
        std::string dirName = splittedPath.back();
        splittedPath.pop_back();
        std::string resultPath = serialize(splittedPath);
        if (!INodeExists(sfile, ROOT_NODE_ID, DIR, resultPath)) {
            std::cout << PATH_NOT_FOUND << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        if (!INodeExists(sfile, sfile->actINodeId, FILES, dirName)) {
            std::cout << PATH_NOT_FOUND << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        ReadAndPrintCurrentINodeContent(sfile);
        sfile->actINodeId = currentINodeId;
        sfile->fsFile.close();
    }

    void Printer::ReadAndPrintCurrentINodeContent(SFile* sfile) {
        INode* iNode = new INode;
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (sfile->actINodeId - 1) * sizeof(INode));
        sfile->fsFile.read((char*) iNode, sizeof(INode));
        std::queue<int32_t> linksQueue;
        std::queue<int32_t> dataQueue;
        for (int j = 1; j <= L_COUNT; j++) {
            Magician::LoadLinkDataIntoQueue(sfile, iNode, &dataQueue, false, j);
        }
        for (int j = 1; j <= L_IN_COUNT; j++) {
            Magician::LoadLinkDataIntoQueue(sfile, iNode, &linksQueue, true, j);
            while (!linksQueue.empty()) {
                int32_t linkId = linksQueue.front();
                linksQueue.pop();
                Magician::LoadIndirectLinks(sfile, &dataQueue, linkId);
            }
        }
        PrintQueueData(&dataQueue, iNode->fileSize);
        std::cout << std::endl;
    }

    void Printer::PrintQueueData(std::queue<int32_t>* dataQueue, long fileSize) {
        int32_t dato;
        char* str;
        for (int i = 0; i < fileSize / sizeof(int32_t); i++) {
            dato = dataQueue->front();
            dataQueue->pop();
            str = (char*)&dato;
            for (int i = 0; i < 4; ++i, ++str) std::cout << *str;
        }
        if (!dataQueue->empty()) {
            dato = dataQueue->front();
            dataQueue->pop();
            str = (char*)&dato;
            for (int i = 0; i < fileSize % sizeof(int32_t); ++i, ++str) std::cout << *str;
        }
    }

    void Printer::PrintNodeInfo(SFile* sfile, std::string path) {
        if (path.compare("") == 0) {
            std::cout << PATH_NOT_FOUND << std::endl;
            return;
        }
        int currentINodeId = sfile->actINodeId;
        sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::in | std::ios::out);
        if (!INodeExists(sfile, ROOT_NODE_ID, OTHER, path)) {
            std::cout << FILE_NOT_FOUND_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        int nodeIdCheckpoint = sfile->actINodeId;
        INode* node = new INode;
        std::string info("");
        if (sfile->actINodeId == ROOT_NODE_ID) std::cout << ROOT_CHAR;
        else {
            std::list<std::string> splittedPath = split(path, DIR_CHAR);
            if (splittedPath.size() > 0 && splittedPath.back().compare("") == 0) splittedPath.pop_back();
            if (splittedPath.size() > 0 && splittedPath.front().compare("~") == 0) splittedPath.pop_front();
            std::string dirName = splittedPath.back();
            std::cout << dirName;
        }
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (nodeIdCheckpoint - 1) * sizeof(INode));
        sfile->fsFile.read((char*) node, sizeof(INode));
        std::cout << " - " << node->fileSize << "B";
        std::cout << " - i-node " << node->nodeId;
        
        std::cout << " - DIRECT:";
        if (node->direct1 != FREE || node->direct2 != FREE || node->direct3 != FREE || node->direct4 != FREE || node->direct5 != FREE) {
            if (node->direct1 != FREE) std::cout << " " << node->direct1;
            if (node->direct2 != FREE) std::cout << " " << node->direct2;
            if (node->direct3 != FREE) std::cout << " " << node->direct3;
            if (node->direct4 != FREE) std::cout << " " << node->direct4;
            if (node->direct5 != FREE) std::cout << " " << node->direct5;
        }
        else std::cout << " none";
        std::cout << " - INDIRECT:";
        if (node->indirect1 != FREE || node->indirect2 != FREE) {
            if (node->indirect1 != FREE) std::cout << " " << node->indirect1;
            if (node->direct2 != FREE) std::cout << " " << node->indirect2;
        }
        else std::cout << " none";
        std::cout << std::endl;
        sfile->actINodeId = currentINodeId;
        sfile->fsFile.close();
    }



}