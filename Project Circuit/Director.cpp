#include <string>
#include "SFile.h"
#include "Director.h"
#include "Convertor.h"
#include "Splitter.h"
#include "Magician.h"

namespace ZOS {

	void Director::Make(SFile* sfile, std::string path) {
        if (path.compare("") == 0) {
            std::cout << PATH_NOT_FOUND << std::endl;
            return;
        }
        sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::out | std::ios::in);
        int currentINodeId = sfile->actINodeId;
        std::list<std::string> splittedPath = split(path, DIR_CHAR);
        if (splittedPath.size() > 0 && splittedPath.back().compare("") == 0) splittedPath.pop_back();
        if (splittedPath.size() > 0 && splittedPath.front().compare("~") == 0) splittedPath.pop_front();
        std::string dirName = splittedPath.back();
        splittedPath.pop_back();
        std::string resultPath = serialize(splittedPath);
        if (!INodeExists(sfile, ROOT_NODE_ID, DIR, resultPath)) {
            std::cout << PATH_NOT_FOUND << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        if (INodeExists(sfile, sfile->actINodeId, DIR, dirName)) {
            std::cout << EXISTS_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        INode* node = new INode;
        for (int i = 0; i < sfile->superBlock->clusterCount / 2; i++) {
            sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + i * sizeof(INode));
            sfile->fsFile.read((char*) node, sizeof(INode));
            if (node->nodeId == FREE) {
                node->nodeId = i + 1;
                node->isDirectory = true;
                node->fileSize = 0;
                sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + i * sizeof(INode));
                sfile->fsFile.write((char*) node, sizeof(INode));
                DirectoryItem* item = new DirectoryItem;
                item->inode = i + 1;
                FillName(item, dirName);
                InitPrintingDirectoryItem(sfile, item);
                break;
            }
        }

        std::cout << OK_RESULT << std::endl;
        sfile->actINodeId = currentINodeId;
        sfile->fsFile.close();
	}

	void Director::Remove(SFile* sfile, std::string path) {
        if (path.compare("") == 0) {
            std::cout << PATH_NOT_FOUND << std::endl;
            return;
        }
        sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::out | std::ios::in);
        int currentINodeId = sfile->actINodeId;
        std::list<std::string> splittedPath = split(path, DIR_CHAR);
        if (splittedPath.size() > 0 && splittedPath.back().compare("") == 0) splittedPath.pop_back();
        if (splittedPath.size() > 0 && splittedPath.front().compare("~") == 0) splittedPath.pop_front();
        std::string dirName = splittedPath.back();
        splittedPath.pop_back();
        std::string resultPath = serialize(splittedPath);
        if (!INodeExists(sfile, ROOT_NODE_ID, DIR, resultPath)) {
            std::cout << PATH_NOT_FOUND << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        int32_t parentINodeId = sfile->actINodeId;
        if (!INodeExists(sfile, sfile->actINodeId, DIR, dirName)) {
            std::cout << FILE_NOT_FOUND_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        INode* nodeToDelete = new INode;
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (sfile->actINodeId - 1) * sizeof(INode));
        sfile->fsFile.read((char*) nodeToDelete, sizeof(INode));
        if (nodeToDelete->fileSize > 0) {
            std::cout << NOT_EMPTY_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        nodeToDelete->nodeId = FREE;
        nodeToDelete->isDirectory = false;
        nodeToDelete->references = FREE;
        nodeToDelete->fileSize = FREE;
        nodeToDelete->direct1 = FREE;
        nodeToDelete->direct2 = FREE;
        nodeToDelete->direct3 = FREE;
        nodeToDelete->direct4 = FREE;
        nodeToDelete->direct5 = FREE;
        nodeToDelete->indirect1 = FREE;
        nodeToDelete->indirect2 = FREE;
        sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (sfile->actINodeId - 1) * sizeof(INode));
        sfile->fsFile.write((char*) nodeToDelete, sizeof(INode));
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (parentINodeId - 1) * sizeof(INode));
        sfile->fsFile.read((char*) nodeToDelete, sizeof(INode));
        for (int i = 1; i <= L_COUNT; i++) {
            DeleteDirectLinks(sfile, sfile->actINodeId, nodeToDelete, i);
        }
        for (int i = 1; i <= L_IN_COUNT; i++) {
            DeleteIndirectLinks(sfile, sfile->actINodeId, nodeToDelete, i);
        }
        std::cout << OK_RESULT << std::endl;
        sfile->actINodeId = currentINodeId;
        sfile->fsFile.close();
	}

    void Director::InitPrintingDirectoryItem(ZOS::SFile* sfile, DirectoryItem* item) {
        INode* node = new INode;
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (sfile->actINodeId - 1) * sizeof(INode));
        sfile->fsFile.read((char*) node, sizeof(INode));
        node->fileSize += sizeof(DirectoryItem);
        bool finded = false;
        for (int i = 1; i <= L_COUNT; i++)   
            if (FindDirectLink(sfile, i, node, item)) {
                finded = true;
                break;
            }
        if (!finded) {
            for (int i = 1; i <= L_IN_COUNT; i++)
                if (FindIndirectLink(sfile, i, node, item))
                    break;
        }

        sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (sfile->actINodeId - 1) * sizeof(INode));
        sfile->fsFile.write((char*) node, sizeof(INode));
        sfile->fsFile.flush();
    }

    DirectoryItem Director::DeleteDirectLinks(SFile* sfile, int32_t iNodeId, INode* node, int32_t directId) {
        int32_t directLinkId = FREE;
        DirectoryItem* dirItem = new DirectoryItem;
        DirectoryItem* emptyDirItem = new DirectoryItem;
        emptyDirItem->inode = 0;
        for (int j = 0; j < DIR_NAME_LENGHT; j++) emptyDirItem->itemName[j] = '\0';
        if (directId == L1) directLinkId = node->direct1;
        else if (directId == L2) directLinkId = node->direct2;
        else if (directId == L3) directLinkId = node->direct3;
        else if (directId == L4) directLinkId = node->direct4;
        else if (directId == L5) directLinkId = node->direct5;
        if (directLinkId != FREE) {
            sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (directLinkId - 1) * DATA_C_SIZE);
            for (unsigned int i = 0; i < LINK_C; i++) {
                sfile->fsFile.read((char*) dirItem, sizeof(DirectoryItem));
                if (dirItem->inode == iNodeId) {
                    sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (directLinkId - 1) * DATA_C_SIZE + i * sizeof(DirectoryItem));
                    sfile->fsFile.write((char*) emptyDirItem, sizeof(DirectoryItem));
                    node->fileSize -= sizeof(DirectoryItem);
                    if (Magician::IsDirectClusterClear(sfile, directLinkId)) {
                        if (directId == L1) node->direct1 = FREE;
                        else if (directId == L2) node->direct2 = FREE;
                        else if (directId == L3) node->direct3 = FREE;
                        else if (directId == L4) node->direct4 = FREE;
                        else if (directId == L5) node->direct5 = FREE;
                        sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + (directLinkId - 1));
                        sfile->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                    }
                    sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node->nodeId - 1) * sizeof(INode));
                    sfile->fsFile.write((char*) node, sizeof(INode));
                    return *dirItem;
                }
            }
        }
        return *emptyDirItem;
    }

    DirectoryItem Director::DeleteIndirectLinks(SFile* sfile, int32_t iNodeId, INode* node, int32_t indirectId) {
        int32_t indirectLinkId = FREE;
        DirectoryItem* dirItem = new DirectoryItem;
        DirectoryItem* emptyDirItem = new DirectoryItem;
        emptyDirItem->inode = FREE;
        for (int i = 0; i < DIR_NAME_LENGHT; i++) emptyDirItem->itemName[i] = '\0';
        if (indirectId == L1) indirectLinkId = node->indirect1;
        else if (indirectId == L2) indirectLinkId = node->indirect2;
        if (indirectLinkId != FREE) {
            int32_t iNodeIndex = 0;
            for (int j = 0; j < IND_LINK_C; j++) {
                sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (indirectLinkId - 1) * DATA_C_SIZE + j * sizeof(int32_t));
                sfile->fsFile.read((char*) &iNodeIndex, sizeof(int32_t));
                if (iNodeIndex > FREE) {
                    for (int i = 0; i < LINK_C; i++) {
                        sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (iNodeIndex - 1) * DATA_C_SIZE + i * sizeof(DirectoryItem));
                        sfile->fsFile.read((char*) dirItem, sizeof(DirectoryItem));
                        if (dirItem->inode == iNodeId) {
                            sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (iNodeIndex - 1) * DATA_C_SIZE + i * sizeof(DirectoryItem));
                            sfile->fsFile.write((char*) emptyDirItem, sizeof(DirectoryItem));
                            node->fileSize -= sizeof(DirectoryItem);
                            if (j == 0 && iNodeIndex == 1 && i == 1) {
                                sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + indirectLinkId - 1);
                                sfile->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                                if (indirectId == L1) node->indirect1 = 0;
                                else if (indirectId == L2) node->indirect2 = 0;
                            }
                            sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node->nodeId - 1) * sizeof(INode));
                            sfile->fsFile.write((char*) node, sizeof(INode));
                            if (i == 1) {
                                sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (indirectLinkId - 1) * DATA_C_SIZE + j * sizeof(int32_t));
                                sfile->fsFile.write((char*) FREE, sizeof(int32_t));
                            }
                            return *dirItem;
                        }
                    }
                }
            }
        }
        return *emptyDirItem;
    }

}