#pragma once
#include <string>
#include "SFile.h"

namespace ZOS {

	class Filer {

	public:
		void static Copy(SFile* sfile, std::string from, std::string to);
		void static Move(SFile* sfile, std::string from, std::string to);
		void static Remove(SFile* sfile, std::string path);
		void static Import(SFile* sfile, std::string from, std::string to);
		bool static FileExists(std::string path);
		void static WriteNewFile(SFile* sfile, std::string from, int32_t nodeId, std::string fileName, int32_t fileSize);
		void static Export(SFile* sfile, std::string from, std::string to);
		void static WriteNewExportFile(SFile* sfile, std::string name);

	};

}