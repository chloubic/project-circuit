﻿#pragma once
#include <cstdint>

struct SuperBlock {
    int32_t diskSize;
    int32_t clusterSize;
    int32_t clusterCount;
    int32_t bitmapStartAddress;
    int32_t inodeStartAddress;
    int32_t dataStartAddress;
};