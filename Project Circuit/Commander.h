#pragma once
#include <list>
#include <string>
#include "SFile.h"

namespace ZOS {

	class Commander {

	private:

		SFile* sfile;

		bool runner;

		void ExecuteCopyCommand(std::list<std::string> command);

		void ExecuteMoveCommand(std::list<std::string> command);

		void ExecuteRemoveCommand(std::list<std::string> command);

		void ExecuteMakeDirCommand(std::list<std::string> command);

		void ExecuteRemoveDirCommand(std::list<std::string> command);

		void ExecuteListCommand(std::list<std::string> command);

		void ExecutePrintCommand(std::list<std::string> command);

		void ExecuteChangeDirCommand(std::list<std::string> command);

		void ExecutePrintWorkingDirCommand(std::list<std::string> command);

		void ExecuteFSInfoCommand(std::list<std::string> command);

		void ExecuteImportCommand(std::list<std::string> command);

		void ExecuteExportCommand(std::list<std::string> command);

		void ExecuteLoadCmdsCommand(std::list<std::string> command);

		void ExecuteFormatCommand(std::list<std::string> command);

		void ExecuteDefragCommand();

		void ExecuteExitCommand();

		void ExecuteCommandNotFound(std::string command);

		int ExtractMultiplier(std::string fileSizeFormat);

	public:

		Commander(SFile* c_sfile);

		void PrintCLIChars();

		void RunCommand(std::string command);

		void Execute();

	};

}