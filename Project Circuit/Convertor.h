#pragma once
#include <string>
#include "DirectoryItem.h"
#include "SFile.h"

void FillName(DirectoryItem* c_dirItem, std::string c_name);

std::string RelativePathToAbsolute(ZOS::SFile* sfile, std::string path);

bool INodeExists(ZOS::SFile* sfile, int32_t iNodeId, int nodeType, std::string path);

INode GetFileNode(ZOS::SFile* sfile, int iNodeId);

DirectoryItem GetFileDirectoryItem(ZOS::SFile* sfile, int directId, int blockPosition);

int32_t GetFileIndirectId(ZOS::SFile* sfile, int indirectId, int blockPosition);

bool FindDirectLink(ZOS::SFile* sfile, int32_t linkType, INode* node, DirectoryItem* item);

bool FindIndirectLink(ZOS::SFile* sfile, int32_t linkType, INode* node, DirectoryItem* item);

void WriteNewLink(ZOS::SFile* sfile, int32_t linkType, INode* node, DirectoryItem* item);

int FindEmptyCluster(ZOS::SFile* sfile);

void WriteDirectoryItem(ZOS::SFile* sfile, DirectoryItem* item, int address);

void WriteIndirectItem(ZOS::SFile* sfile, int linkType, int linkIndex, DirectoryItem* item);