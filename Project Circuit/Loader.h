#pragma once
#include <string>
#include "Commander.h"

namespace ZOS {

	class Loader {

	public:

		void static Load(Commander* commander, std::string fileName);

	};

}