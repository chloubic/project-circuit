#include <string>
#include <iostream>
#include <fstream>
#include "Loader.h"
#include "Constants.h"
#include "Utils.h"
#include "Commander.h"

namespace ZOS {

	void Loader::Load(Commander* commander, std::string fileName) {
		if (!Utils::FileExists(fileName)) std::cout << FILE_NOT_FOUND_RESULT << std::endl;
		else {
            std::ifstream commandsFile;
            commandsFile.open(fileName.c_str(), std::ios::in);
            std::string command = "";
            while (!commandsFile.eof()) {
                commander->PrintCLIChars();
                getline(commandsFile, command);
                std::cout << command << std::endl;
                commander->RunCommand(command);
            }
            commandsFile.close();
			std::cout << OK_RESULT << std::endl;
		}
	}

}