#pragma once

#include <string>
#include <list>

std::list<std::string> split(std::string s, std::string delimiter);

std::string serialize(std::list<std::string> list);