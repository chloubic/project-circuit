#pragma once
#include "SFile.h"

namespace ZOS {

	class Formatter {

	public:

		static void Format(SFile* sfile, int bytes);

	};

}