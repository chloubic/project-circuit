#include <string>
#include <queue>
#include <list>
#include "Filer.h"
#include "Splitter.h"
#include "Convertor.h"
#include "Constants.h"
#include "Director.h"
#include "Magician.h"

namespace ZOS {

	void Filer::Copy(SFile* sfile, std::string from, std::string to) {
        if (from.compare("") == 0 || to.compare("") == 0) {
            std::cout << PATH_NOT_FOUND << std::endl;
            return;
        }
        sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::out | std::ios::in);
        int currentINodeId = sfile->actINodeId;
        std::list<std::string> splittedFromPath = split(from, DIR_CHAR);
        if (splittedFromPath.size() > 0 && splittedFromPath.back().compare("") == 0) splittedFromPath.pop_back();
        if (splittedFromPath.size() > 0 && splittedFromPath.front().compare("~") == 0) splittedFromPath.pop_front();
        std::string dirNameFrom = splittedFromPath.back();
        splittedFromPath.pop_back();
        std::string resultFromPath = serialize(splittedFromPath);


        std::list<std::string> splittedToPath = split(to, DIR_CHAR);
        if (splittedToPath.size() > 0 && splittedToPath.back().compare("") == 0) splittedToPath.pop_back();
        if (splittedToPath.size() > 0 && splittedToPath.front().compare("~") == 0) splittedToPath.pop_front();
        std::string dirNameTo = splittedToPath.back();
        splittedToPath.pop_back();
        std::string resultToPath = serialize(splittedToPath);
        if (!INodeExists(sfile, ROOT_NODE_ID, DIR, resultFromPath)) {
            std::cout << FILE_NOT_FOUND_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        if (!INodeExists(sfile, sfile->actINodeId, FILES, dirNameFrom)) {
            std::cout << FILE_NOT_FOUND_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        int nodeId = sfile->actINodeId;
        if (!INodeExists(sfile, ROOT_NODE_ID, DIR, resultToPath)) {
            std::cout << PATH_NOT_FOUND << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        int targetNodeId = sfile->actINodeId;
        if (INodeExists(sfile, sfile->actINodeId, FILES, dirNameTo)) {
            std::cout << EXISTS_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        Magician::DuplicateNodes(sfile, nodeId, targetNodeId, dirNameTo);
        std::cout << OK_RESULT << std::endl;
        sfile->actINodeId = currentINodeId;
        sfile->fsFile.close();
	}

	void Filer::Move(SFile* sfile, std::string from, std::string to) {
        if (from.compare("") == 0 || to.compare("") == 0) {
            std::cout << PATH_NOT_FOUND << std::endl;
            return;
        }
        sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::out | std::ios::in);
        int currentINodeId = sfile->actINodeId;
        std::list<std::string> splittedFromPath = split(from, DIR_CHAR);
        if (splittedFromPath.size() > 0 && splittedFromPath.back().compare("") == 0) splittedFromPath.pop_back();
        if (splittedFromPath.size() > 0 && splittedFromPath.front().compare("~") == 0) splittedFromPath.pop_front();
        std::string dirNameFrom = splittedFromPath.back();
        splittedFromPath.pop_back();
        std::string resultFromPath = serialize(splittedFromPath);


        std::list<std::string> splittedToPath = split(to, DIR_CHAR);
        if (splittedToPath.size() > 0 && splittedToPath.back().compare("") == 0) splittedToPath.pop_back();
        if (splittedToPath.size() > 0 && splittedToPath.front().compare("~") == 0) splittedToPath.pop_front();
        std::string dirNameTo = splittedToPath.back();
        splittedToPath.pop_back();
        std::string resultToPath = serialize(splittedToPath);
        if (!INodeExists(sfile, ROOT_NODE_ID, DIR, resultFromPath)) {
            std::cout << FILE_NOT_FOUND_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        int nodeId = sfile->actINodeId;
        if (!INodeExists(sfile, ROOT_NODE_ID, DIR, resultToPath)) {
            std::cout << PATH_NOT_FOUND << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        int targetNodeId = sfile->actINodeId;
        if (!INodeExists(sfile, nodeId, FILES, dirNameFrom)) {
            std::cout << FILE_NOT_FOUND_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        if (INodeExists(sfile, targetNodeId, FILES, dirNameTo)) {
            std::cout << EXISTS_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        DirectoryItem dirItem = Magician::TakeFile(sfile, nodeId);
        FillName(&dirItem, dirNameTo);
        sfile->actINodeId = targetNodeId;
        Director::InitPrintingDirectoryItem(sfile, &dirItem);
        std::cout << OK_RESULT << std::endl;
        sfile->actINodeId = currentINodeId;
        sfile->fsFile.close();
	}

	void Filer::Remove(SFile* sfile, std::string path) {
        if (path.compare("") == 0) {
            std::cout << PATH_NOT_FOUND << std::endl;
            return;
        }
        sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::out | std::ios::in);
        int currentINodeId = sfile->actINodeId;
        std::list<std::string> splittedPath = split(path, DIR_CHAR);
        if (splittedPath.size() > 0 && splittedPath.back().compare("") == 0) splittedPath.pop_back();
        if (splittedPath.size() > 0 && splittedPath.front().compare("~") == 0) splittedPath.pop_front();
        std::string dirName = splittedPath.back();
        splittedPath.pop_back();
        std::string resultPath = serialize(splittedPath);
        if (!INodeExists(sfile, ROOT_NODE_ID, DIR, resultPath)) {
            std::cout << FILE_NOT_FOUND_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        int parentIdCheckpoint = sfile->actINodeId;
        if (!INodeExists(sfile, sfile->actINodeId, FILES, dirName)) {
            std::cout << FILE_NOT_FOUND_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        Magician::RemoveINodes(sfile, sfile->actINodeId);
        INode* node = new INode;
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (parentIdCheckpoint - 1) * sizeof(INode));
        sfile->fsFile.read((char*) node, sizeof(INode));
        for (int i = 1; i <= L_COUNT; i++) Magician::DeleteDirectLinks(sfile, sfile->actINodeId, node, i);
        for (int i = 1; i <= L_IN_COUNT; i++) Magician::DeleteIndirectLinks(sfile, sfile->actINodeId, node, i);
        std::cout << OK_RESULT << std::endl;
        sfile->actINodeId = currentINodeId;
        sfile->fsFile.close();
	}

	void Filer::Import(SFile* sfile, std::string from, std::string to) {
		struct stat stat_buf;
		int32_t fileSize = FREE;
		if (!FileExists(from)) {
			std::cout << FILE_NOT_FOUND_RESULT << std::endl;
			return;
		}
		if (to.compare("") == 0) {
			std::cout << PATH_NOT_FOUND << std::endl;
			return;
		}
		sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::out | std::ios::in);
		int currentINodeId = sfile->actINodeId;
		std::list<std::string> splittedPath = split(to, DIR_CHAR);
		if (splittedPath.size() > 0 && splittedPath.back().compare("") == 0) splittedPath.pop_back();
		if (splittedPath.size() > 0 && splittedPath.front().compare("~") == 0) splittedPath.pop_front();
		std::string dirName = splittedPath.back();
		splittedPath.pop_back();
		std::string resultPath = serialize(splittedPath);
		if (!INodeExists(sfile, ROOT_NODE_ID, DIR, resultPath)) {
			std::cout << PATH_NOT_FOUND << std::endl;
			sfile->actINodeId = currentINodeId;
			sfile->fsFile.close();
			return;
		}
		if (INodeExists(sfile, sfile->actINodeId, FILES, dirName)) {
			std::cout << EXISTS_RESULT << std::endl;
			sfile->actINodeId = currentINodeId;
			sfile->fsFile.close();
			return;
		}
		int rc = stat(from.c_str(), &stat_buf);
		if (rc == 0) fileSize = stat_buf.st_size;
		else {
			std::cout << FILE_NOT_FOUND_RESULT << std::endl;
			sfile->actINodeId = currentINodeId;
			sfile->fsFile.close();
			return;
		}
		WriteNewFile(sfile, from, sfile->actINodeId, dirName, fileSize);
		std::cout << OK_RESULT << std::endl;
		sfile->actINodeId = currentINodeId;
		sfile->fsFile.close();
	}

	void Filer::WriteNewFile(SFile* sfile, std::string from, int32_t nodeId, std::string fileName, int32_t fileSize) {
        std::ifstream fileToImport;
        int32_t fileData;
        INode* iNode = new INode;
        fileToImport.open(from, std::ios::binary | std::ios::out | std::ios::in);
        for (int i = 0; i < sfile->superBlock->clusterCount / 2; i++) {
            sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + i * sizeof(INode));
            sfile->fsFile.read((char*) iNode, sizeof(INode));
            if (iNode->nodeId == FREE) {
                iNode->nodeId = i + 1;
                iNode->isDirectory = false;
                iNode->references = 1;
                iNode->direct1 = 0;
                iNode->direct2 = 0;
                iNode->direct3 = 0;
                iNode->direct4 = 0;
                iNode->direct5 = 0;
                iNode->indirect1 = 0;
                iNode->indirect2 = 0;
                iNode->fileSize = 0;
                DirectoryItem* dirItem = new DirectoryItem;
                dirItem->inode = iNode->nodeId;
                FillName(dirItem, fileName);
                Director::InitPrintingDirectoryItem(sfile, dirItem);
                std::queue<int32_t> dataQueue;
                for (int j = 1; j <= L_COUNT; j++) {
                    if (fileSize >= sfile->superBlock->clusterSize) {
                        for (int k = 0; k < sfile->superBlock->clusterSize / sizeof(int32_t); k++) {
                            fileToImport.read((char*) &fileData, sizeof(int32_t));
                            dataQueue.push(fileData);
                        }
                        Magician::WriteDataIntoCluster(sfile, iNode, &dataQueue, false, j);
                        iNode->fileSize += sfile->superBlock->clusterSize;
                        fileSize -= sfile->superBlock->clusterSize;
                    }
                    else if (fileSize != FREE) {
                        for (int k = 0; k <= fileSize / sizeof(int32_t); k++) {
                            if (fileSize % sizeof(int32_t) == 0 && k == (fileSize / sizeof(int32_t))) {}
                            else {
                                fileToImport.read((char*)&fileData, sizeof(int32_t));
                                dataQueue.push(fileData);
                            }
                        }

                        Magician::WriteDataIntoCluster(sfile, iNode, &dataQueue, false, j);
                        iNode->fileSize += fileSize;
                        fileSize = FREE;
                    }
                }
                for (int j = 1; j <= L_IN_COUNT; j++) {
                    for (int m = 0; m < IND_LINK_C; m++) {
                        if (fileSize >= sfile->superBlock->clusterSize) {
                            if (m == FREE) Magician::CreateIndirectForImportedFile(sfile, iNode, j);
                            for (int k = 0; k < sfile->superBlock->clusterSize / sizeof(int32_t); k++) {
                                fileToImport.read((char*) &fileData, sizeof(int32_t));
                                dataQueue.push(fileData);
                            }
                            Magician::WriteDataIntoCluster(sfile, iNode, &dataQueue, true, j);
                            iNode->fileSize += sfile->superBlock->clusterSize;
                            fileSize -= sfile->superBlock->clusterSize;
                        }
                        else if (fileSize != FREE) {
                            if (m == FREE) Magician::CreateIndirectForImportedFile(sfile, iNode, j);
                            for (int k = 0; k <= fileSize / sizeof(int32_t); k++) {
                                if (fileSize % sizeof(int32_t) == 0 && k == (fileSize / sizeof(int32_t))) {}
                                else {
                                    fileToImport.read((char*)&fileData, sizeof(int32_t));
                                    dataQueue.push(fileData);
                                }
                            }
                            Magician::WriteDataIntoCluster(sfile, iNode, &dataQueue, true, j);
                            iNode->fileSize += fileSize;
                            fileSize = FREE;
                        }
                    }
                }
                sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + i * sizeof(INode));
                sfile->fsFile.write((char*) iNode, sizeof(INode));
                sfile->fsFile.flush();
                fileToImport.close();
                break;
            }
        }
	}

	bool Filer::FileExists(std::string path) {
		std::ifstream sfile(path);
		return sfile.good();
	}

    void Filer::Export(SFile* sfile, std::string from, std::string to) {
        // Check for path existence....
        if (from.compare("") == 0) {
            std::cout << PATH_NOT_FOUND << std::endl;
            return;
        }
        sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::out | std::ios::in);
        int currentINodeId = sfile->actINodeId;
        std::list<std::string> splittedPath = split(from, DIR_CHAR);
        if (splittedPath.size() > 0 && splittedPath.back().compare("") == 0) splittedPath.pop_back();
        if (splittedPath.size() > 0 && splittedPath.front().compare("~") == 0) splittedPath.pop_front();
        std::string dirName = splittedPath.back();
        splittedPath.pop_back();
        std::string resultPath = serialize(splittedPath);
        if (!INodeExists(sfile, ROOT_NODE_ID, DIR, resultPath)) {
            std::cout << PATH_NOT_FOUND << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }
        if (!INodeExists(sfile, sfile->actINodeId, FILES, dirName)) {
            std::cout << FILE_NOT_FOUND_RESULT << std::endl;
            sfile->actINodeId = currentINodeId;
            sfile->fsFile.close();
            return;
        }

        WriteNewExportFile(sfile, to);
        std::cout << OK_RESULT << std::endl;
        sfile->actINodeId = currentINodeId;
        sfile->fsFile.close();
    }

    void Filer::WriteNewExportFile(SFile* sfile, std::string name) {
        std::ofstream exportedFile;
        exportedFile.open(name, std::ios::binary | std::ios::out);
        INode* iNode = new INode;
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (sfile->actINodeId - 1) * sizeof(INode));
        sfile->fsFile.read((char*) iNode, sizeof(INode));
        std::queue<int32_t> linksQueue;
        std::queue<int32_t> dataQueue;
        int32_t dataSegmentId;
        for (int j = 1; j <= L_COUNT; j++)
            Magician::LoadLinkDataIntoQueue(sfile, iNode, &dataQueue, false, j);
        for (int j = 1; j <= L_IN_COUNT; j++) {
            Magician::LoadLinkDataIntoQueue(sfile, iNode, &linksQueue, true, j);
            while (!linksQueue.empty()) {
                int32_t linkId = linksQueue.front();
                linksQueue.pop();
                Magician::LoadIndirectLinks(sfile, &dataQueue, linkId);
            }
        }
        int32_t dato;
        char* str;
        for (int i = 0; i < iNode->fileSize / sizeof(int32_t); i++) {
            dato = dataQueue.front();
            dataQueue.pop();
            str = (char*)&dato;
            for (int i = 0; i < sizeof(int32_t); ++i, ++str) exportedFile.write((char*)str, sizeof(char));
        }
        if (!dataQueue.empty()) {
            dato = dataQueue.front();
            dataQueue.pop();
            str = (char*)&dato;
            for (int i = 0; i < iNode->fileSize % sizeof(int32_t); ++i, ++str) exportedFile.write((char*)str, sizeof(char));
        }
        exportedFile.flush();
        exportedFile.close();
    }

}