#include "Formatter.h"
#include "SFile.h"

namespace ZOS {

	void Formatter::Format(SFile* sfile, int bytes) {
		sfile->InitFSWithSize(bytes);
		sfile->Initialize();
		sfile->ValidateSFile();
	}

}