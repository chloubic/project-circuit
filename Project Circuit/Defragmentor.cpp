#include <queue>
#include <list>
#include "SFile.h"
#include "Defragmentor.h"
#include "Constants.h"
#include "Magician.h"

namespace ZOS {

	void Defragmentor::Defrag(SFile* sfile) {
        int32_t actNodeId = 1;
        int32_t oldLinkId;
        char oldBitmapChar;
        std::queue<int32_t> newDataQueue;
        std::queue<int32_t> oldDataQueue;
        std::queue<int32_t> linksQueue;
        sfile->fsFile.open(sfile->fsName, std::ios::binary | std::ios::in | std::ios::out);
        std::list<INode> nodes;
        Magician::LoadINodesIntoList(sfile, &nodes);
        INode newNode;
        while (actNodeId < sfile->superBlock->clusterCount && !nodes.empty()) {
            newNode = nodes.front();
            nodes.pop_front();
            for (int j = 1; j <= L_COUNT; j++) {
                if (Magician::LoadLinkDataIntoQueue(sfile, &newNode, &newDataQueue, false, j)) {
                    Magician::LoadClusterDataIntoQueue(sfile, &oldDataQueue, actNodeId);
                    if (j == L1) {
                        oldLinkId = newNode.direct1;
                        newNode.direct1 = actNodeId + 1;
                    }
                    else if (j == L2) {
                        oldLinkId = newNode.direct2;
                        newNode.direct2 = actNodeId + 1;
                    }
                    else if (j == L3) {
                        oldLinkId = newNode.direct3;
                        newNode.direct3 = actNodeId + 1;
                    }
                    else if (j == L4) {
                        oldLinkId = newNode.direct4;
                        newNode.direct4 = actNodeId + 1;
                    }
                    else if (j == L5) {
                        oldLinkId = newNode.direct5;
                        newNode.direct5 = actNodeId + 1;
                    }
                    Magician::SortINodeData(sfile, &nodes, actNodeId + 1, oldLinkId);
                    sfile->fsFile.seekg(sfile->superBlock->bitmapStartAddress + actNodeId);
                    sfile->fsFile.read((char*) &oldBitmapChar, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                    sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + actNodeId);
                    sfile->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                    sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + (oldLinkId - 1));
                    sfile->fsFile.write((char*) &oldBitmapChar, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                    sfile->fsFile.flush();
                    Magician::SortClusterData(sfile, &oldDataQueue, &newDataQueue, actNodeId + 1, oldLinkId);
                    actNodeId++;
                }
            }
            for (int j = 1; j <= L_IN_COUNT; j++) {
                if (Magician::LoadLinkDataIntoQueue(sfile, &newNode, &newDataQueue, true, j)) {
                    linksQueue = newDataQueue;
                    Magician::LoadClusterDataIntoQueue(sfile, &oldDataQueue, actNodeId);
                    if (j == L1){
                        oldLinkId = newNode.indirect1;
                        newNode.indirect1 = actNodeId + 1;
                    }
                    else if (j == L2) {
                        oldLinkId = newNode.indirect2;
                        newNode.indirect2 = actNodeId + 1;
                    }
                    Magician::SortINodeData(sfile, &nodes, actNodeId + 1, oldLinkId);
                    sfile->fsFile.seekg(sfile->superBlock->bitmapStartAddress + actNodeId);
                    sfile->fsFile.read((char*) &oldBitmapChar, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                    sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + actNodeId);
                    sfile->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                    sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + (oldLinkId - 1));
                    sfile->fsFile.write((char*) &oldBitmapChar, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                    sfile->fsFile.flush();
                    Magician::SortClusterData(sfile, &oldDataQueue, &newDataQueue, actNodeId + 1, oldLinkId);
                    int32_t fromClusterId = actNodeId;
                    actNodeId++;
                    for (int k = 0; !linksQueue.empty(); k++) {
                        oldLinkId = linksQueue.front();
                        linksQueue.pop();
                        Magician::LoadClusterDataIntoQueue(sfile, &newDataQueue, oldLinkId - 1);
                        Magician::LoadClusterDataIntoQueue(sfile, &oldDataQueue, actNodeId);
                        sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + fromClusterId * DATA_C_SIZE + k * sizeof(int32_t));
                        int32_t newLinkId = actNodeId + 1;
                        sfile->fsFile.write((char*) &newLinkId, sizeof(int32_t));
                        Magician::SortINodeData(sfile, &nodes, newLinkId, oldLinkId);
                        sfile->fsFile.seekg(sfile->superBlock->bitmapStartAddress + actNodeId);
                        sfile->fsFile.read((char*) &oldBitmapChar, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                        sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + actNodeId);
                        sfile->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                        sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + (oldLinkId - 1));
                        sfile->fsFile.write((char*)&oldBitmapChar, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                        sfile->fsFile.flush();
                        Magician::SortClusterData(sfile, &oldDataQueue, &newDataQueue, newLinkId, oldLinkId);
                        actNodeId++;
                    }
                }
            }
            sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (newNode.nodeId - 1) * sizeof(INode));
            sfile->fsFile.write((char*) &newNode, sizeof(INode));
        }
        sfile->fsFile.close();
        std::cout << OK_RESULT << std::endl;
	}

}