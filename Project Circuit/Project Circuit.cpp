// Project Circuit.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <string>
#include "Constants.h"
#include "SFile.h"
#include "Convertor.h"
#include "Commander.h"

int main(int argc, char* argv[])
{
	if (argc != 2) {
		std::cout << "Enter FS name as first parameter" << std::endl;
		exit(WRONG_ARGS);
	}

	std::string FSName(argv[1]);

	ZOS::SFile* sfile = new ZOS::SFile(FSName);
	ZOS::Commander* commander = new ZOS::Commander(sfile);

	if (!sfile->SFileExists()) sfile->InitFSWithSize(FS_INIT_SIZE);
	else sfile->Initialize();
	
	commander->Execute();

	delete commander;
	delete sfile;

	return 0;
}