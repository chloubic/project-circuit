﻿#pragma once
#include <cstdint>
    struct INode {
        int32_t nodeId;
        bool isDirectory;
        int8_t references;
        int32_t fileSize;
        int32_t direct1;
        int32_t direct2;
        int32_t direct3;
        int32_t direct4;
        int32_t direct5;
        int32_t indirect1;
        int32_t indirect2;
    };

    struct Comparator {
        bool operator ()(const INode& c_inode1, const INode& c_inode2) {
            return c_inode1.fileSize > c_inode2.fileSize;
        }
    };
