#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "SFile.h"
#include "Constants.h"
#include "SuperBlock.h"
#include "DirectoryItem.h"
#include "INode.h"

namespace ZOS {

	SFile::SFile(std::string fsName) {
		this->fsName = fsName;
		this->superBlock = new SuperBlock;
		this->actINodeId = 1;
		this->actPath = std::string(ROOT_CHAR).append("/");
	}

	SFile::~SFile() {
		delete this->superBlock;
	}

	void SFile::Initialize() {
		if (!this->SFileExists()) return;
		this->fsFile.open(this->fsName, std::ios::binary | std::ios::out | std::ios::in);
		this->fsFile.read((char*) &(superBlock->diskSize), sizeof(int32_t));
		this->fsFile.read((char*) &(superBlock->clusterSize), sizeof(int32_t));
		this->fsFile.read((char*) &(superBlock->clusterCount), sizeof(int32_t));
		this->fsFile.read((char*) &(superBlock->bitmapStartAddress), sizeof(int32_t));
		this->fsFile.read((char*) &(superBlock->inodeStartAddress), sizeof(int32_t));
		this->fsFile.read((char*) &(superBlock->dataStartAddress), sizeof(int32_t));
		this->fsFile.close();
	}

	void SFile::InitFSWithSize(int c_fsSize) {
		if (this->SFileExists()) this->DeleteSFile();
		int uninitializedSize = c_fsSize - sizeof(SuperBlock);
		superBlock->diskSize = c_fsSize;
		superBlock->clusterSize = DATA_C_SIZE;
		superBlock->bitmapStartAddress = sizeof(SuperBlock);
		superBlock->clusterCount = (int32_t)(uninitializedSize / (1 + 0.5 * sizeof(INode) + superBlock->clusterSize));
		uninitializedSize -= superBlock->clusterCount;
		superBlock->inodeStartAddress = superBlock->bitmapStartAddress + superBlock->clusterCount;
		superBlock->dataStartAddress = (int32_t)(superBlock->inodeStartAddress + 0.5 * sizeof(INode) * superBlock->clusterCount);
		WriteFS();
	}

	void SFile::WriteFS() {
		std::ofstream(this->fsName);
		this->fsFile.open(this->fsName, std::ios::binary | std::ios::out | std::ios::in);
		INode* initialNode = new INode{0, false, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		WriteInitSuperBlock();
		WriteInitBitMap();
		WriteInitINodeBlock(initialNode);
		WriteInitDataBlock();
		this->fsFile.seekp(superBlock->inodeStartAddress);
		initialNode->nodeId = 1;
		initialNode->isDirectory = true;
		initialNode->references = 1;
		this->fsFile.write((char*) initialNode, sizeof(INode));
		this->fsFile.flush();
		this->fsFile.close();
	}

	void SFile::ValidateSFile() {
		if (this->fsFile.good()) std::cout << OK_RESULT << std::endl;
		else {
			std::cout << CANNOT_CR_FILE_RESULT << std::endl;
		}
	}

	void SFile::WriteInitSuperBlock() {
		this->fsFile.write((char*) &(superBlock->diskSize), sizeof(int32_t));
		this->fsFile.write((char*) &(superBlock->clusterSize), sizeof(int32_t));
		this->fsFile.write((char*) &(superBlock->clusterCount), sizeof(int32_t));
		this->fsFile.write((char*) &(superBlock->bitmapStartAddress), sizeof(int32_t));
		this->fsFile.write((char*) &(superBlock->inodeStartAddress), sizeof(int32_t));
		this->fsFile.write((char*) &(superBlock->dataStartAddress), sizeof(int32_t));
	}

	void SFile::WriteInitBitMap() {
		for (int i = 0; i < superBlock->clusterCount; i++) {
			this->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
		}
	}

	void SFile::WriteInitINodeBlock(INode* initialNode) {
		for (int i = 0; i < superBlock->clusterCount * 0.5; i++) this->fsFile.write((char*) initialNode, sizeof(INode));
		this->fsFile.flush();
	}

	void SFile::WriteInitDataBlock() {
		int32_t initialValue = 0;
		for (int i = 0; i < superBlock->clusterCount; i++)
			for (int j = 0; j < IND_LINK_C; j++)
				this->fsFile.write((char*) &initialValue, sizeof(int32_t));
	}

	void SFile::DeleteSFile() {
		if (remove(this->fsName.c_str()) != 0) exit(FS_FILE_EXCEPTION);
	}

	bool SFile::SFileExists() {
		std::ifstream sfile(this->fsName);
		return sfile.good();
	}

	std::string SFile::GetFSName() { return this->fsName; }
	std::string SFile::GetPath() { return this->actPath; }

}