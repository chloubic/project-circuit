#include <queue>
#include <list>
#include "Magician.h"
#include "INode.h"
#include "SFile.h"
#include "Director.h"
#include "Constants.h"
#include "Convertor.h"

int32_t ZERO = 0;

namespace ZOS {

	void Magician::WriteDataIntoCluster(SFile* sfile, INode* node, std::queue<int32_t>* dataQueue, bool isIndirect, int32_t linkId) {
        if (dataQueue->empty()) return;
        int emptyClusterIndex = FindEmptyCluster(sfile);
        if (!isIndirect) {
            if (linkId == L1) node->direct1 = emptyClusterIndex;
            else if (linkId == L2) node->direct2 = emptyClusterIndex;
            else if (linkId == L3) node->direct3 = emptyClusterIndex;
            else if (linkId == L4) node->direct4 = emptyClusterIndex;
            else if (linkId == L5) node->direct5 = emptyClusterIndex;
        }
        else {
            int indirectLinkId = FREE;
            if (linkId == L1) indirectLinkId = node->indirect1;
            else if (linkId == L2) indirectLinkId = node->indirect2;
            int32_t indirectLinkValue = FREE;
            sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (indirectLinkId - 1) * DATA_C_SIZE);
            for (int j = 0; j < IND_LINK_C; j++) {
                sfile->fsFile.read((char*) &indirectLinkValue, sizeof(int32_t));
                if (indirectLinkValue == FREE) {
                    sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (indirectLinkId - 1) * DATA_C_SIZE + j * sizeof(int32_t));
                    sfile->fsFile.write((char*) &emptyClusterIndex, sizeof(int32_t));
                    sfile->fsFile.flush();
                    break;
                }
            }
        }
        sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (emptyClusterIndex - 1) * DATA_C_SIZE);
        while (!dataQueue->empty()) {
            int32_t segmentData = dataQueue->front();
            dataQueue->pop();
            sfile->fsFile.write((char*) &segmentData, sizeof(int32_t));
            sfile->fsFile.flush();
        }
	}

    void Magician::CreateIndirectForImportedFile(SFile* sfile, INode* node, int32_t linkId) {
        int emptyClusterIndex = FindEmptyCluster(sfile);
        if (linkId == L1) node->indirect1 = emptyClusterIndex;
        else if (linkId == L2) node->indirect2 = emptyClusterIndex;
    }

    bool Magician::LoadLinkDataIntoQueue(SFile* sfile, INode* node, std::queue<int32_t>* dataQueue, bool isIndirect, int32_t linkId) {
        int32_t actLinkId = FREE;
        int32_t dataSegmentId = FREE;
        if (!isIndirect) {
            if (linkId == L1) actLinkId = node->direct1;
            else if (linkId == L2) actLinkId = node->direct2;
            else if (linkId == L3) actLinkId = node->direct3;
            else if (linkId == L4) actLinkId = node->direct4;
            else if (linkId == L5) actLinkId = node->direct5;
        }
        else {
            if (linkId == L1) actLinkId = node->indirect1;
            else if (linkId == L2) actLinkId = node->indirect2;
        }

        if (actLinkId != FREE) {
            sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (actLinkId - 1) * DATA_C_SIZE);
            for (int i = 0; i < IND_LINK_C; i++) {
                sfile->fsFile.read((char*) &dataSegmentId, sizeof(int32_t));
                if (dataSegmentId != FREE) dataQueue->push(dataSegmentId);
                else break;
            }
            return true;
        }
        else return false;
    }

    void Magician::LoadIndirectLinks(SFile* sfile, std::queue<int32_t>* dataQueue, int32_t linkId) {
        int32_t dataSegmentId;
        sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (linkId - 1) * DATA_C_SIZE);
        for (int i = 0; i < IND_LINK_C; i++) {
            sfile->fsFile.read((char*) &dataSegmentId, sizeof(int32_t));
            if (dataSegmentId != FREE) dataQueue->push(dataSegmentId);
            else break;
        }
    }

    DirectoryItem Magician::DeleteDirectLinks(SFile* sfile, int32_t iNodeId, INode* node, int32_t directId) {
        int32_t directLinkId = FREE;
        DirectoryItem* dirItem = new DirectoryItem;
        DirectoryItem* emptyDirItem = new DirectoryItem;
        emptyDirItem->inode = FREE;
        for (int i = 0; i < DIR_NAME_LENGHT; i++) emptyDirItem->itemName[i] = '\0';
        if (directId == L1) directLinkId = node->direct1;
        else if (directId == L2) directLinkId = node->direct2;
        else if (directId == L3) directLinkId = node->direct3;
        else if (directId == L4) directLinkId = node->direct4;
        else if (directId == L5) directLinkId = node->direct5;
        if (directLinkId != FREE) {
            sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (directLinkId - 1) * DATA_C_SIZE);
            for (int i = 0; i < LINK_C; i++) {
                sfile->fsFile.read((char*) dirItem, sizeof(DirectoryItem));
                if (dirItem->inode == iNodeId) {
                    sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (directLinkId - 1) * DATA_C_SIZE + i * sizeof(DirectoryItem));
                    sfile->fsFile.write((char*) emptyDirItem, sizeof(DirectoryItem));
                    node->fileSize -= sizeof(DirectoryItem);
                    if (Magician::IsDirectClusterClear(sfile, directLinkId)) {
                        if (directId == L1) node->direct1 = 0;
                        else if (directId == L2) node->direct2 = 0;
                        else if (directId == L3) node->direct3 = 0;
                        else if (directId == L4) node->direct4 = 0;
                        else if (directId == L5) node->direct5 = 0;
                        sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + (directLinkId - 1));
                        sfile->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                    }
                    sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node->nodeId - 1) * sizeof(INode));
                    sfile->fsFile.write((char*) node, sizeof(INode));
                    return *dirItem;
                }
            }
        }
        return *emptyDirItem;
    }

    DirectoryItem Magician::DeleteIndirectLinks(SFile* sfile, int32_t iNodeId, INode* node, int32_t indirectId) {
        int32_t indirectLinkId = FREE;
        DirectoryItem* dirItem = new DirectoryItem;
        DirectoryItem* emptyDirItem = new DirectoryItem;
        emptyDirItem->inode = FREE;
        for (int i = 0; i < DIR_NAME_LENGHT; i++) emptyDirItem->itemName[i] = '\0';
        if (indirectId == L1) indirectLinkId = node->indirect1;
        else if (indirectId == L2) indirectLinkId = node->indirect2;
        if (indirectLinkId != FREE) {
            int32_t dataIndex = FREE;
            for (int j = 0; j < IND_LINK_C; j++) {
                sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (indirectLinkId - 1) * DATA_C_SIZE + j * sizeof(int32_t));
                sfile->fsFile.read((char*) &dataIndex, sizeof(int32_t));
                if (dataIndex > FREE) {
                    for (int i = 0; i < LINK_C; i++) {
                        sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (dataIndex - 1) * DATA_C_SIZE + i * sizeof(DirectoryItem));
                        sfile->fsFile.read((char*) dirItem, sizeof(DirectoryItem));
                        if (dirItem->inode == iNodeId) {
                            sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (dataIndex - 1) * DATA_C_SIZE + i * sizeof(DirectoryItem));
                            sfile->fsFile.write((char*) emptyDirItem, sizeof(DirectoryItem));
                            node->fileSize -= sizeof(DirectoryItem);
                            if (j == 0 && dataIndex == 1 && i == 1) {
                                sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + indirectLinkId - 1);
                                sfile->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
                                if (indirectId == L1) node->indirect1 = FREE;
                                else if (indirectId == L2) node->indirect2 = FREE;
                            }
                            sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node->nodeId - 1) * sizeof(INode));
                            sfile->fsFile.write((char*) node, sizeof(INode));

                            if (i == 1) {
                                sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (indirectLinkId - 1) * DATA_C_SIZE + j * sizeof(int32_t));
                                sfile->fsFile.write((char*) FREE, sizeof(int32_t));
                            }
                            return *dirItem;
                        }
                    }
                }
            }
        }
        return *emptyDirItem;
    }

    void Magician::RemoveINodes(SFile* sfile, int32_t iNodeId) {
        INode* node = new INode;
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (iNodeId - 1) * sizeof(INode));
        sfile->fsFile.read((char*) node, sizeof(INode));
        for (int i = 1; i <= L_COUNT; i++) RemoveDirects(sfile, node, i);
        for (int i = 1; i <= L_IN_COUNT; i++) RemoveIndirects(sfile, node, i);
        node->nodeId = 0;
        node->isDirectory = false;
        node->references = 0;
        node->fileSize = 0;
        node->direct1 = 0;
        node->direct2 = 0;
        node->direct3 = 0;
        node->direct4 = 0;
        node->direct5 = 0;
        node->indirect1 = 0;
        node->indirect2 = 0;
        sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (iNodeId - 1) * sizeof(INode));
        sfile->fsFile.write((char*) node, sizeof(INode));
        sfile->fsFile.flush();
    }

    void Magician::RemoveDirects(SFile* sfile, INode* node, int32_t directId) {
        int32_t directLinkId = FREE;
        if (directId == L1) directLinkId = node->direct1;
        else if (directId == L2) directLinkId = node->direct2;
        else if (directId == L3) directLinkId = node->direct3;
        else if (directId == L4) directLinkId = node->direct4;
        else if (directId == L5) directLinkId = node->direct5;
        if (directLinkId != FREE){
            sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (directLinkId - 1) * DATA_C_SIZE);
            for (int j = 0; j < IND_LINK_C; j++) sfile->fsFile.write((char*) &ZERO, sizeof(int32_t));
            sfile->fsFile.flush();
            sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + (directLinkId - 1));
            sfile->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
            sfile->fsFile.flush();
        }
    }

    void Magician::RemoveIndirects(SFile* sfile, INode* node, int32_t indirectId) {
        int32_t indirectLinkId;
        if (indirectId == L1) indirectLinkId = node->indirect1;
        else if (indirectId == L2) indirectLinkId = node->indirect2;
        if (indirectLinkId != FREE) {
            int32_t dataIndex = 0;
            for (int j = 0; j < IND_LINK_C; j++) {
                sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (indirectLinkId - 1) * DATA_C_SIZE + j * sizeof(int32_t));
                sfile->fsFile.read((char*) &dataIndex, sizeof(int32_t));
                if (dataIndex > FREE) {
                    sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (dataIndex - 1) * DATA_C_SIZE);
                    for (int k = 0; k < IND_LINK_C; k++) sfile->fsFile.write((char*) FREE, sizeof(int32_t));
                    sfile->fsFile.flush();
                    sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + (dataIndex - 1));
                    sfile->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);

                    sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (indirectLinkId - 1) * DATA_C_SIZE + j * sizeof(int32_t));
                    sfile->fsFile.write((char*) &ZERO, sizeof(int32_t));
                    sfile->fsFile.flush();
                }
            }
            sfile->fsFile.seekp(sfile->superBlock->bitmapStartAddress + (indirectLinkId - 1));
            sfile->fsFile.write(DEFAULT_BITMAP_VALUE, DEFAULT_BITMAP_VALUE_STREAM_SIZE);
            sfile->fsFile.flush();
        }
    }

    DirectoryItem Magician::TakeFile(SFile* sfile, int32_t nodeId) {
        DirectoryItem dirItem;
        dirItem.inode = FREE;
        for (int i = 0; i < DIR_NAME_LENGHT; i++) dirItem.itemName[i] = '\0';
        INode* node = new INode;
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (nodeId - 1) * sizeof(INode));
        sfile->fsFile.read((char*) node, sizeof(INode));
        for (int i = 1; i <= L_COUNT; i++) {
            dirItem = DeleteDirectLinks(sfile, sfile->actINodeId, node, i);
            return dirItem;
        }
        for (int i = 1; i <= L_IN_COUNT; i++) {
            dirItem = DeleteIndirectLinks(sfile, sfile->actINodeId, node, i);
            return dirItem;
        }
        return dirItem;
    }

    void Magician::DuplicateNodes(SFile* sfile, int32_t iNodeIdFrom, int32_t iNodeIdTo, std::string name) {
        INode* fromNode = new INode;
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + (iNodeIdFrom - 1) * sizeof(INode));
        sfile->fsFile.read((char*) fromNode, sizeof(INode));
        INode* emptyNode = new INode;
        for (int i = 0; i < sfile->superBlock->clusterCount; i++) {
            sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + i * sizeof(INode));
            sfile->fsFile.read((char*) emptyNode, sizeof(INode));
            if (emptyNode->nodeId == FREE) {
                emptyNode->nodeId = i + 1;
                emptyNode->isDirectory = false;
                emptyNode->fileSize = fromNode->fileSize;
                emptyNode->references = 1;
                emptyNode->direct1 = 0;
                emptyNode->direct2 = 0;
                emptyNode->direct3 = 0;
                emptyNode->direct4 = 0;
                emptyNode->direct5 = 0;
                emptyNode->indirect1 = 0;
                emptyNode->indirect2 = 0;
                std::queue<int32_t> linksQueue;
                std::queue<int32_t> dataQueue;
                for (int j = 1; j <= L_COUNT; j++) {
                    LoadLinkDataIntoQueue(sfile, fromNode, &dataQueue, false, j);
                    WriteDataIntoCluster(sfile, emptyNode, &dataQueue, false, j);
                }
                for (int j = 1; j <= L_IN_COUNT; j++) {
                    LoadLinkDataIntoQueue(sfile, fromNode, &linksQueue, true, j);
                    if (!linksQueue.empty()) CreateIndirectForImportedFile(sfile, emptyNode, j);
                    while (!linksQueue.empty()) {
                        int32_t link = linksQueue.front();
                        linksQueue.pop();
                        LoadIndirectLinks(sfile, &dataQueue, link);
                        if (!dataQueue.empty()) WriteDataIntoCluster(sfile, emptyNode, &dataQueue, true, j);
                    }
                }
                sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + i * sizeof(INode));
                sfile->fsFile.write((char*) emptyNode, sizeof(INode));
                break;
            }
        }
        DirectoryItem* dirItem = new DirectoryItem;
        dirItem->inode = emptyNode->nodeId;
        FillName(dirItem, name);
        sfile->actINodeId = iNodeIdTo;
        Director::InitPrintingDirectoryItem(sfile, dirItem);
    }

    void Magician::LoadClusterDataIntoQueue(SFile* sfile, std::queue<int32_t>* dataQueue, int32_t clusterId) {
        int32_t segmentData;
        sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + clusterId * DATA_C_SIZE);
        for (int i = 0; i < IND_LINK_C; i++) {
            sfile->fsFile.read((char*) &segmentData, sizeof(int32_t));
            if (segmentData != FREE) dataQueue->push(segmentData);
            else break;
        }
    }

    void Magician::LoadINodesIntoList(SFile* sfile, std::list<INode>* nodesList) {
        INode* node = new INode;
        sfile->fsFile.seekg(sfile->superBlock->inodeStartAddress + sizeof(INode));
        for (int i = 1; i < sfile->superBlock->clusterCount / 2; i++) {
            sfile->fsFile.read((char*) node, sizeof(INode));
            if (node->nodeId > ROOT_NODE_ID) nodesList->push_front(*node);
        }
        nodesList->sort(Comparator());
    }

    void Magician::SortClusterData(SFile* sfile, std::queue<int32_t>* oldDataQueue, std::queue<int32_t>* newDataQueue, int32_t oldClusterId, int32_t newClusterId) {
        int32_t segmentData;
        sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (newClusterId - 1) * sfile->superBlock->clusterSize);
        for (int i = 0; i < IND_LINK_C; i++) {
            if (!oldDataQueue->empty()) {
                segmentData = oldDataQueue->front();
                oldDataQueue->pop();
                sfile->fsFile.write((char*) &segmentData, sizeof(int32_t));
            }
            else {
                segmentData = FREE;
                sfile->fsFile.write((char*) &segmentData, sizeof(int32_t));
            }
        }
        sfile->fsFile.flush();
        sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (oldClusterId - 1) * sfile->superBlock->clusterSize);
        for (int i = 0; i < IND_LINK_C; i++) {
            if (!newDataQueue->empty()) {
                segmentData = newDataQueue->front();
                newDataQueue->pop();
                sfile->fsFile.write((char*) &segmentData, sizeof(int32_t));
            }
            else {
                segmentData = FREE;
                sfile->fsFile.write((char*) &segmentData, sizeof(int32_t));
            }
        }
        sfile->fsFile.flush();
    }

    void Magician::SortINodeData(SFile* sfile, std::list<INode>* nodesList, int32_t oldNodeId, int32_t newNodeId) {
        INode node;
        int32_t indirectLinkId;
        bool founded = false;
        for (int i = 0; i < nodesList->size(); i++) {
            node = nodesList->front();
            nodesList->pop_front();
            if (!founded && node.direct1 == oldNodeId) {
                node.direct1 = newNodeId;
                sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node.nodeId - 1) * sizeof(INode));
                sfile->fsFile.write((char*) &node, sizeof(INode));
                founded = true;
            }
            else if (!founded && node.direct2 == oldNodeId) {
                node.direct2 = newNodeId;
                sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node.nodeId - 1) * sizeof(INode));
                sfile->fsFile.write((char*) &node, sizeof(INode));
                founded = true;
            }
            else if (!founded && node.direct3 == oldNodeId) {
                node.direct3 = newNodeId;
                sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node.nodeId - 1) * sizeof(INode));
                sfile->fsFile.write((char*) &node, sizeof(INode));
                founded = true;
            }
            else if (!founded && node.direct4 == oldNodeId) {
                node.direct4 = newNodeId;
                sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node.nodeId - 1) * sizeof(INode));
                sfile->fsFile.write((char*) &node, sizeof(INode));
                founded = true;
            }
            else if (!founded && node.direct5 == oldNodeId) {
                node.direct5 = newNodeId;
                sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node.nodeId - 1) * sizeof(INode));
                sfile->fsFile.write((char*) &node, sizeof(INode));
                founded = true;
            }
            else if (!founded && node.indirect1 == oldNodeId) {
                node.indirect1 = newNodeId;
                sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node.nodeId - 1) * sizeof(INode));
                sfile->fsFile.write((char*) &node, sizeof(INode));
                founded = true;
            }
            else if (!founded && node.indirect2 == oldNodeId) {
                node.indirect2 = newNodeId;
                sfile->fsFile.seekp(sfile->superBlock->inodeStartAddress + (node.nodeId - 1) * sizeof(INode));
                sfile->fsFile.write((char*) &node, sizeof(INode));
                founded = true;
            }
            if (!founded && node.indirect1 != 0) {
                sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (node.indirect1 - 1) * DATA_C_SIZE);
                for (int j = 0; j < IND_LINK_C; j++) {
                    sfile->fsFile.read((char*) &indirectLinkId, sizeof(int32_t));
                    if (indirectLinkId == oldNodeId) {
                        sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (node.indirect1 - 1) * DATA_C_SIZE + j * sizeof(int32_t));
                        sfile->fsFile.write((char*) &newNodeId, sizeof(int32_t));
                        sfile->fsFile.flush();
                        founded = true;
                        break;
                    }
                }
            }
            else if (!founded && node.indirect2 != 0) {
                sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (node.indirect2 - 1) * DATA_C_SIZE);
                for (int j = 0; j < IND_LINK_C; j++) {
                    sfile->fsFile.read((char*) &indirectLinkId, sizeof(int32_t));
                    if (indirectLinkId == oldNodeId) {
                        sfile->fsFile.seekp(sfile->superBlock->dataStartAddress + (node.indirect2 - 1) * DATA_C_SIZE + j * sizeof(int32_t));
                        sfile->fsFile.write((char*) &newNodeId, sizeof(int32_t));
                        sfile->fsFile.flush();
                        founded = true;
                        break;
                    }
                }
            }

            nodesList->push_back(node);
        }
        sfile->fsFile.flush();
    }

    bool Magician::IsDirectClusterClear(SFile* sfile, int32_t directLinkId) {
        bool isEmpty = true;
        DirectoryItem* dirItem = new DirectoryItem;
        sfile->fsFile.seekg(sfile->superBlock->dataStartAddress + (directLinkId - 1) * DATA_C_SIZE);
        for (int i = 0; i < LINK_C; i++) {
            sfile->fsFile.read((char*)dirItem, sizeof(DirectoryItem));
            if (dirItem->inode != FREE) isEmpty = false;
        }
        return isEmpty;
    }
}