#include "Splitter.h"
#include "Constants.h"
#include <string>
#include <list>

std::list<std::string> split(std::string s, std::string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::list<std::string> res;

    while ((pos_end = s.find(delimiter, pos_start)) != std::string::npos) {
        token = s.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back(token);
    }

    res.push_back(s.substr(pos_start));
    return res;
}

std::string serialize(std::list<std::string> list) {
    std::string serialized = "";
    std::list<std::string>::iterator it;
    for (it = list.begin(); it != list.end(); ++it) {
        serialized.append((*it) + DIR_CHAR);
    }
    return serialized;
}