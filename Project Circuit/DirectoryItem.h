#pragma once
#include <cstdint>
#include "Constants.h"

#define DIR_NAME_LENGHT 12

struct DirectoryItem {
    int32_t inode;
    char itemName[DIR_NAME_LENGHT];
};